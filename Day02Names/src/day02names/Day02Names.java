package day02names;

import java.util.ArrayList;
import java.util.Scanner;

public class Day02Names {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<String> nameList = new ArrayList<>();
        String name = "";
        do {
            System.out.println("Please enter a name, empty to exit: ");
            name = input.nextLine();
            if (!name.equals("")) {
                nameList.add(name);
            }
        } while (!name.equals(""));
        // print them all out one per line
        for (String n : nameList) {
            System.out.println("Name: " + n);
        }
    }
    
}