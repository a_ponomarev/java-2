
package rectangleobj;

class Rectangle {

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    
    public Rectangle(){
    
    }
    
    public double width =1;
    public double height =1;
    
    public double getArea(){
        return width*height;
    }
    
    public double getPerimeter(){
        return 2*(width+height);
    }
}

public class RectangleObj {

    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(4,40);
        Rectangle rect2 = new Rectangle(3.5, 35.9);
        System.out.printf(
        "Rectangle 1 width: %.2f, height: %.2f, area:%.2f, perimeter: %.2f", rect1.width, rect1.height,  rect1.getArea(), rect1.getPerimeter());
        System.out.println();
        System.out.printf(
        "Rectangle 2 width: %.2f, height: %.2f, area:%.2f, perimeter: %.2f", rect2.width, rect2.height,  rect2.getArea(), rect2.getPerimeter());
        System.out.println();
    }
    
}
