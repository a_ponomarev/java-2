
package accountobj;

import java.util.Date;

class Account {

    public Date getDateCreated() {
        return dateCreated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public void setAnnualInterestRate(double annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }

private int id = 0;
private double balance = 0;
private double annualInterestRate = 0;
private Date dateCreated = new Date(System.currentTimeMillis());
    
public Account() {

}

public Account(int id, double balance, double annualInterestRate) {
    this.id = id;
    this.balance = balance;
    this.annualInterestRate = annualInterestRate;
    this.dateCreated = new Date(System.currentTimeMillis());
}

public double getMonthlyInterestRate(){
    return annualInterestRate/12;
}

public double getMonthlyInterest(){

    return balance*this.getAnnualInterestRate();
}
    
public void withdraw(double substract){
   this.balance -= substract;
    
} 

public void deposit(double add){
    this.balance += add;
}

}

public class AccountObj {

    public static void main(String[] args) {
        Account nAcc = new Account(1122,20000,0.045);
        
        System.out.printf("Account %d's balance is %.2f with the annual rate of %.1f percent. Date created is %s", nAcc.getId(), nAcc.getBalance(),nAcc.getAnnualInterestRate()*100, nAcc.getDateCreated().toString());
        System.out.println();
        System.out.println("Monthly interest " + nAcc.getMonthlyInterest());
        System.out.println();
        nAcc.withdraw(2500);
        nAcc.deposit(3000);
        System.out.printf("Account %d's balance is %.2f with the annual rate of %.1f percent. Date created is %s", nAcc.getId(), nAcc.getBalance(),nAcc.getAnnualInterestRate()*100, nAcc.getDateCreated().toString());
         System.out.println();
        System.out.println("Monthly interest " + nAcc.getMonthlyInterest());
    }
    
}
