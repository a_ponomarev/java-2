
package gregoriancalendar;

import java.util.GregorianCalendar;

public class GregorianCalendarExample {

    public static void main(String[] args) {
        GregorianCalendar grCal = new GregorianCalendar();
        System.out.println("The present date: " + grCal.get(GregorianCalendar.YEAR)+"/"+grCal.get(GregorianCalendar.MONTH)+"/"+grCal.get(GregorianCalendar.DAY_OF_MONTH));
        grCal.setTimeInMillis(1234567898765L);
        System.out.println("The date after change: "+grCal.get(GregorianCalendar.YEAR)+"/"+grCal.get(GregorianCalendar.MONTH)+"/"+grCal.get(GregorianCalendar.DAY_OF_MONTH));
    }
    
}
