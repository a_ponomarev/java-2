
package outofbounds;

import java.util.Scanner;

public class OutOfBounds {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] qwe = new int[100];
        try {
            System.out.println("Enter index: ");
            int index = input.nextInt();
            System.out.println(qwe[index]);
        } catch (IndexOutOfBoundsException ex){
            System.out.println("Out of bounds");
        }
    }
    
}
