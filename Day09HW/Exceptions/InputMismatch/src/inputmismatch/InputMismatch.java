
package inputmismatch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputMismatch {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (true) {
            try {
            
            System.out.println("Please enter two numbers: ");
            int n1 = input.nextInt();
            int n2 = input.nextInt();
            System.out.println(n1+n2);
            
            } catch (InputMismatchException ex){
                System.out.println("Invalid data: " + ex.getMessage());
                input.nextLine();
            }
            
        
        }
    }
    
}
