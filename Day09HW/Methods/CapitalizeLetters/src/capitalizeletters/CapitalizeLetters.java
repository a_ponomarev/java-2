package capitalizeletters;

import java.util.Scanner;

public class CapitalizeLetters {
   static Scanner in = new Scanner(System.in);
   static String line;
   static String upper_case_line;

    public static String title(String s) {

        
        upper_case_line = "";
        Scanner lineScan = new Scanner(s);
        while (lineScan.hasNext()) {
            String word = lineScan.next();
            upper_case_line += Character.toUpperCase(word.charAt(0)) + word.substring(1) + " ";
        }
        
        return upper_case_line.trim();

    }

    public static void main(String[] args) {
        System.out.print("Input a Sentence: ");
        line = in.nextLine();
        System.out.println(title(line));
    }

}
