
package isbn13;

import java.util.Scanner;

public class ISBN13 {

    public static void main(String[] args) {
       Scanner input = new Scanner(System.in);
        long first12 = 0;
        int sum = 0;
        System.out.println("Enter the first 12 digits of an ISBN as integer: ");
        first12 = input.nextLong();
        long copyFirst12 = first12;
        int length = String.valueOf(first12).length();
        int[] nums = new int[length];
        
        for (int i = 0; i < length; i++) {
            if (i%2==0) {
                nums[i] = (int)(first12%10)*3;
                first12 = first12/10;
            }    
            else {
                nums[i] = (int)(first12%10);
                first12 = first12/10;
            }
        }
        
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        
        if((10-(sum%10))==10)
            System.out.println("The ISBN-13 number is "+copyFirst12+"0");
        else
            System.out.println("The ISBN-13 number is "+copyFirst12+(10-(sum%10)));
    }
    
}
