
package binarytodecimal;

import java.util.Scanner;

public class BinaryToDecimal {
    
    public static int binaryToDecimal(String binaryString){
        
        int sum = 0;
        for (int i = 1; i <= binaryString.length(); i++) {
            sum += Character.getNumericValue(binaryString.charAt(i-1))*Math.pow(2, (binaryString.length()-i));
        }
        
        return sum;
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a binary string: ");
        String str = input.nextLine();
        System.out.println("The sum is " + binaryToDecimal(str));
    }
    
}
