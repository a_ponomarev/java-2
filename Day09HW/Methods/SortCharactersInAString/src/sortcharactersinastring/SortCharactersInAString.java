package sortcharactersinastring;

public class SortCharactersInAString {

    public static char[] sort(String s) {
        char[] str = new char[s.length()];
        // Fill array with the elements of the string
        for (int i = 0; i < str.length; i++) {
            str[i] = s.charAt(i);
        }

        // Sort array
        for (int i = 0; i < str.length - 1; i++) {
            char min = str[i];
            int minIndex = i;

            for (int j = i + 1; j < str.length; j++) {
                if (min > str[j]) {
                    min = str[j];
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                str[minIndex] = str[i];
                str[i] = min;
            }
        }
        return str;
    }

    public static void main(String[] args) {
        System.out.println(sort("acb"));
    }

}
