
package circle2dobject;

class Circle2D{

    private double x = 0;
    private double y = 0;
    private double radius = 1;
    
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getRadius() {
        return radius;
    }

    public Circle2D() {
    }

    
    
    public Circle2D(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
    
    public double getArea(){
        return Math.PI * Math.pow(radius, 2);
    }
    public double getPerimeter(){
        return 2 * Math.PI * radius;
    }
    
    public boolean contains(double x, double y){
        return Math.sqrt(Math.pow(x - this.x, 2) + 
            Math.pow(y - this.y, 2)) < radius;
    }
    
    /** Return true if the specified 
	*   circle is inside this circle */
	public boolean contains(Circle2D circle) {
		return Math.sqrt(Math.pow(circle.getX() - x, 2) + 
				 Math.pow(circle.getY() - y, 2)) 
				 <= Math.abs(radius - circle.getRadius());
	}

	/** Return true if the specified 
	*   circle overlaps with this circle */
	public boolean overlaps(Circle2D circle) {
		return Math.sqrt(Math.pow(circle.getX() - x, 2) + 
				 Math.pow(circle.getY() - y, 2)) 
				 <= radius + circle.getRadius();
}
}

public class Circle2DObject {

    public static void main(String[] args) {
	// Create a Circle2D object
		Circle2D c1 = new Circle2D(2, 2, 5.5);

		// Display results
		System.out.println("Circle1 area: " + c1.getArea()); 
		System.out.println("Circle1 perimeter: " + c1.getPerimeter()); 
		System.out.println(
			"Does circle1 contain the point (3, 3)? " + c1.contains(3, 3)); 
		System.out.println(
			"Does circle1 contain the circle centered at (4, 5) and radius 10.5? " 
			+ c1.contains(new Circle2D(4, 5, 10.5)));
		System.out.println(
			"Does circle1 overlap the circle centered at (3, 5) and radius 2.3? " 
+ c1.overlaps(new Circle2D(3, 5, 2.3)));
    }
    
}
