
package myintegerobj;

class MyInteger {

    public MyInteger(int value) {
        this.value = value;
    }
    
    private int value;
    
    public int getValue(){
     return value;
    }
    
    public boolean isEven(int value){
      if (value%2==0) {
          return true;
      }
      else 
          return false;
    }
    
    public boolean isOdd(int value){
      if (value%2==1) {
          return true;
      }
      else 
          return false;
    }
    
    public static boolean isPrime(int N) {
        if (N < 2) return false;

        for (int i = 2; i*i <= N; i++)
            if (N % i == 0) return false;

        return true;
    }
    
    public boolean isEven(MyInteger obj){
      if (obj.value%2==0) {
          return true;
      }
      else 
          return false;
    }
    
    public boolean isOdd(MyInteger obj){
      if (obj.value%2==1) {
          return true;
      }
      else 
          return false;
    }
    
    public static boolean isPrime(MyInteger obj) {
        if (obj.value < 2) return false;

        for (int i = 2; i*i <= obj.value; i++)
            if (obj.value % i == 0) return false;

        return true;
    }
    
    public boolean equals(int value){
        return this.value == value;
    }
    
    public boolean equals(MyInteger obj){
        return this.value == obj.value;
    }
}

public class MyIntegerObj {

    public static void main(String[] args) {
        MyInteger myInt = new MyInteger(51);
        System.out.println(myInt.isEven(myInt));
    }
    
}
