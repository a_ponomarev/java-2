
package day10peoplefromfile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class ParameterInvalidException extends Exception {
    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Person {

    public Person(String name, int age) throws ParameterInvalidException {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws ParameterInvalidException {
        if (name.length()< 2)
            throw new ParameterInvalidException("Name must be at least 2 ch long");
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws ParameterInvalidException {
        if (age < 0 || age > 150)
            throw new ParameterInvalidException("Age must be between 0 and 150");
        this.age = age;
    }
    private String name;
    private int age;
    
    public void print(){
        System.out.println("Person's name is "+this.name+" and the age is "+ this.age);
    }
    
}

public class Day10PeopleFromFile {
    
    static ArrayList<Person> people = new ArrayList<>();

    public static void main(String[] args) {
        { // Reading from a text file 
            try (Scanner fileInput = new Scanner(new File("people.txt"))) {
                
                
                while (fileInput.hasNextLine()){

                    try {
                        String name = fileInput.nextLine();
                        int age = fileInput.nextInt();
                        fileInput.nextLine();
                        Person p = new Person(name, age);
                        people.add(p);
                    } catch (ParameterInvalidException ex){
                        System.out.println("Invalid data" + ex.getMessage());
                    } catch (java.util.InputMismatchException ex){
                        System.out.println("Invalid data" + ex.getMessage());
                    }
                }
                
            } catch (IOException ex){
                System.out.println("File reading error: "+ex.getMessage());
            } 
            
            // 
            for ( Person person: people) {
                person.print();
            }
        }
        
    }
    
}
