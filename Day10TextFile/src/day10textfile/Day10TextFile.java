
package day10textfile;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class Day10TextFile {
static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        { // Writing to a file
            System.out.println("Enter a line of text: ");
            String line = input.nextLine();
            
            try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("output.txt")))){
              pw.println(line);
              pw.println(line);
              pw.println(line);
              
            } catch (IOException ex) {
                System.out.println("File writing error: " + ex.getMessage());
            }
        }
        { // Reading from a text file 
            try (Scanner fileInput = new Scanner(new File("output.txt"))) {
                while (fileInput.hasNextLine()){
                    String line = fileInput.nextLine();
                    System.out.println("Line retrieved was: "+line);  
                }
                
            } catch (IOException ex){
                System.out.println("File reading error: "+ex.getMessage());
            }
            
        
        }
    }
    
}
