
package day08todos;

import java.util.Scanner;

class Todo {

    public Todo(String task, String dueDate, int hoursOfWork) throws Exception {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) throws Exception {
        if (task.length()<2 || task.length() >50)
            throw new Exception("Task should be between 2 and 50 chars");
        
        this.task = task;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) throws Exception {
        if (dueDate.length()<2 || dueDate.length() >20)
            throw new Exception("Due date should be between 2 and 20 chars");
        this.dueDate = dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) throws Exception {
        if (hoursOfWork<=0)
            throw new Exception("Hours of work should be more than zero");
        this.hoursOfWork = hoursOfWork;
    }
    private String task; // 2-50 chars
    private String dueDate; // 6-20 chars
    private int hoursOfWork; // 0 >
    
    public void print(){
        System.out.println("Todo: "+ this.task+", "+this.dueDate+", will take "+this.hoursOfWork+" hour(s) of work");
    }
}

public class Day08Todos {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter task description: ");
        String task = input.nextLine();
        System.out.println("Enter due date: ");
        String dueDate = input.nextLine();
        System.out.println("Enter hours of work");
        int hoursOfWork = input.nextInt();
        try {
        Todo nTask = new Todo(task, dueDate, hoursOfWork);
        nTask.print();
        } catch (Exception ex){
            System.out.println("Error while creating Todo: "+ex.getMessage());
        }
    }
    
}
