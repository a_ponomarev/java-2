/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01simplecalc;

import java.util.Scanner;

/**
 *
 * @author 1796188
 */
public class Day01SimpleCalc {

    public static void main(String[] args) {
        int option=-1;
        float num1, num2;
        String str1, str2, ttl;
        
        str1 = "Please choose the first number: ";
        str2 = "Please choose the second number: ";
        ttl = "The result is: ";
        
        while (!(option==0)) {
            System.out.println("Welcome to SimpleCalc");
            System.out.println("Please choose the operation:");
            System.out.println("1. Add");
            System.out.println("2. Substract");
            System.out.println("3. Multiply");
            System.out.println("4. Divide");
            System.out.println("0. Exit");
            Scanner input = new Scanner(System.in);
            option = input.nextInt();
            switch (option) {
                case 1: 
                    System.out.println();
                    System.out.println();
                    System.out.println(str1);
                    num1=input.nextFloat();
                    System.out.println(str2);
                    num2=input.nextFloat();
                    System.out.println(ttl+(num1+num2));
                    System.out.println();
                    System.out.println();
                    break;
                case 2: 
                    System.out.println();
                    System.out.println();                    
                    System.out.println(str1);
                    num1=input.nextFloat();
                    System.out.println(str2);
                    num2=input.nextFloat();
                    System.out.println(ttl+((num1>num2)?num1-num2:num2-num1));
                    System.out.println();
                    System.out.println();
                    break;   
                case 3: 
                    System.out.println();
                    System.out.println();                    
                    System.out.println(str1);
                    num1=input.nextFloat();
                    System.out.println(str2);
                    num2=input.nextFloat();
                    System.out.println(ttl+(num1*num2));
                    System.out.println();
                    System.out.println();
                    break;
                case 4: 
                    System.out.println();
                    System.out.println();                    
                    System.out.println(str1);
                    num1=input.nextFloat();
                    System.out.println(str2);
                    num2=input.nextFloat();
                    System.out.println(ttl+((num2==0)?"Invalid operation":num1/num2));
                    System.out.println();
                    System.out.println();
                    break; 
                case 0: 
                    System.exit(0);
                   
                
            }
        }
        
            
        
    }
    
}
