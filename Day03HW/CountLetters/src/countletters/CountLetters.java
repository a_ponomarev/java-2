
package countletters;

public class CountLetters {
    
    public static int countLetters(String s){
        int counter=0;
        for (int i = 0; i < s.length(); i++) {
            
            if (Character.isLetter(s.charAt(i)))
                counter++;
            
        }
        
        return counter;
    }
    
    public static void main(String[] args) {
        System.out.println(countLetters("Hello!12 "));
    }
    
}
