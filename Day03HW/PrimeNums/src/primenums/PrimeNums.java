
package primenums;



public class PrimeNums
{
 public static void main (String[]args)
 {

  int number = 2;
  boolean isPrime = true;
  System.out.println("The prime numbers from 2 to 1000 are \n");
  while (number <= 100)
  {
   isPrime = true;
   for (int divisor = 2; divisor <= number/2; divisor++)
   {
    if (number % divisor == 0)
    {
     isPrime = false;break;
    }
   }
   if (isPrime)
   {
    System.out.print(number + "\t");
    
   }
  number++;
  }
 }
}

