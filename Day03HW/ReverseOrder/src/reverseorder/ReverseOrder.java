/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reverseorder;

import java.util.Scanner;

/**
 *
 * @author Alex
 */
public class ReverseOrder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
        int N1, N2, N3;
        
        System.out.println("Please assign number 1");
        N1 = input.nextInt();
        System.out.println("Please assign number 2");
        N2 = input.nextInt();
        System.out.println("Please assign number 3");
        N3 = input.nextInt();  
        
        System.out.println("Your numbers are: "+ N3+" "+ N2+" "+ N1);
        
    }
    
}
