
package buildmatrix;

import java.util.Scanner;

public class BuildMatrix {
    
    public static void printMatrix(int n) {
        for (int i = 0; i < n; i++) {
            
            for (int j = 0; j < n; j++) {
                int rand = (int)(Math.random()*2);
                System.out.print(rand);
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter n:");
        int n=input.nextInt();
        printMatrix(n);
        
        
    }
    
}
