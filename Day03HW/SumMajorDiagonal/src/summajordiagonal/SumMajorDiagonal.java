
package summajordiagonal;

import java.util.Scanner;

public class SumMajorDiagonal {
    
    public static double sumMajorDiagonal(double[][] m) {
        double diagSum = 0;
        int c=0;
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
               
               diagSum+=m[i][c];
               c++;
               break;
            }
        }
        return diagSum;
    }

    
    public static void main(String[] args) {
        double[][] m = new double[4][4];
        int entry = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 4x4 matrix row by row:");
        
        for (int i = 0; i < m.length; i++) {
            
            for (int j = 0; j < m[i].length; j++) {
                entry = input.nextInt();
                m[i][j]=entry;
                
            }
            
            input.nextLine();
        }
        
        System.out.println(sumMajorDiagonal(m));
        
        
    }
    
}
