
package scissorsrockpaper;

import java.util.Scanner;

public class ScissorsRockPaper {

   
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int val = -1;
        
        String name = "";
        String user = "";
        
        while (true){
            int rand = (int)(Math.random()*3);
            System.out.println("Scissor(0), Rock(1), Paper(2)");
            val = input.nextInt();
            
            if (val==5){
                System.out.println("Thanx for playing");
                break;
            }
            
            if (rand==0)
                name = "Scissor";
            else if (rand==1)
                name = "Rock";
            else if (rand==2)
                name = "Paper";
            
            if (val==0)
                user = "Scissor";
            else if (val==1)
                user = "Rock";
            else if (val==2)
                user = "Paper";
            
            if (rand==val) {
                System.out.println("The computer is " + name + ". You are "+user+". This is draw.");
            }
            else if (rand==0 && val==1)
                System.out.println("The computer is " + name + ". You are "+user+". You win.");
            else if (rand==0 && val==2)
                System.out.println("The computer is " + name + ". You are "+user+". You lose.");    
            else if (rand==1 && val==0)
                System.out.println("The computer is " + name + ". You are "+user+". You lose.");
            else if (rand==1 && val==2)
                System.out.println("The computer is " + name + ". You are "+user+". You won.");
            else if (rand==2 && val==0)
                System.out.println("The computer is " + name + ". You are "+user+". You won."); 
            else if (rand==2 && val==1)
                System.out.println("The computer is " + name + ". You are "+user+". You lose."); 
        }
       
    }
    
}
