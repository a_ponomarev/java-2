
package pointinarectangle;

import java.util.Scanner;

public class PointInARectangle {

    public static void main(String[] args) {
	
        Scanner input = new Scanner(System.in);
        double rectW, rectH;
        String userEntry;
        
        rectW = 10/2;
        rectH = 5.0/2;
        
        System.out.println("Please enter coordinates X and Y separated by space: "); 
        userEntry = input.nextLine();
        
        String[] strs = userEntry.split("\\s+");
        
        double[] a = new double[strs.length];
        
        for (int i = 0; i < strs.length; i++) {
            a[i] = Double.parseDouble(strs[i]);
            System.out.println(a[i]);
        }

        if (a[0]<=rectW && a[1]<=rectH)
            System.out.println("Point " +a[0]+" "+a[1]+" is in the rectangle");
        else
            System.out.println("Point " +a[0]+" "+a[1]+" is not in the rectangle");
	
    }
    
}
