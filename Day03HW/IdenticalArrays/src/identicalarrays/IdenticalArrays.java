
package identicalarrays;


import java.util.Arrays;
import java.util.Scanner;

public class IdenticalArrays {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter the list 1");
	String userEntry = input.nextLine();
        
	System.out.println("Enter the list 2");
	String userEntry2 = input.nextLine();
        
	String[] strs = userEntry.split("\\s+");
	String[] strs2 = userEntry2.split("\\s+");
        
	int[] a = new int[strs.length];
        int[] a2 = new int[strs2.length];
	
	for (int i = 0; i < strs.length; i++) {
		a[i] = Integer.parseInt(strs[i]);
		
	}
        
        for (int i = 0; i < strs2.length; i++) {
		a2[i] = Integer.parseInt(strs[i]);
		
	}
         
        if (equals(a, a2))
            System.out.println("Two lists are identical");
        else 
            System.out.println("Not identical");
            
    }
    
    public static boolean equals(int[] list1, int[] list2) {
    
             
        if ( Arrays.equals(list1, list2))
            return true;
        else
            return false;
        
        
      
    }
    
}
