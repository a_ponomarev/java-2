
package addition;

import java.util.Scanner;

/**
 *
 * @author Alex
 */
public class Addition {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int randN1, randN2;
        int ans = 0;
        
        // assign random
        
        randN1 = (int)(Math.random()*100);
        randN2 = (int)(Math.random()*100);
        
        System.out.println(
                "Please sum up these 2 numbers: " + randN1 +"+"+ randN2);
        ans = input.nextInt();
        if (ans==(randN1+randN2))
            System.out.println("True");
        else
            System.out.println("False");
        
    }
    
}
