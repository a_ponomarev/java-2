
package sumcolumn;

public class SumColumn {
    
    public static double sumColumn(double[][] m, int columnIndex) {
        double colSum = 0;
        for (int col = columnIndex; col == columnIndex; col++) {
            for (int row = 0; row < m.length; row++) {
                colSum+=m[row][col];
            }
        }
        return colSum;
    }

    
    public static void main(String[] args) {
        double[][] m = {
            {1.5, 2, 3, 4},
            {5.5, 6, 7, 8},
            {9.5, 1, 3, 1},
        };
        int col = 4;
        System.out.println("The sum of column "+col+": "+sumColumn(m, col-1));
        
    }
    
}
