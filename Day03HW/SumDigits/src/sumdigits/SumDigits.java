
package sumdigits;

import java.util.Scanner;

public class SumDigits {

    public static int sumDigits(long n){
        int length = String.valueOf(n).length();
        int[] nums = new int[length];
        int sum = 0;
        
        for (int i = 0; i < length; i++) {
            if (i%2==0) {
                nums[i] = (int)(n%10);
                n = n/10;
            }    
            else {
                nums[i] = (int)(n%10);
                n = n/10;
            }
        }
        
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        
//        nums[1] = (int)(n%10);
//        n = n/10;
//        nums[1] = (int)(n%10);
//        nums[2] = (int)(n/10);
        
        
        
        
       
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]+"; ");
        }
  
            
        
        return sum;
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please write down the number to sum up: ");
        int val = input.nextInt();
        System.out.println("\n"+sumDigits(val));
    }
    
}
