
package locationobject;

import java.util.Scanner;

class Location {
    public int row, column;
    public double maxValue;
}

public class LocationObject {
    public static Location locateLargest(double[][] a) {
        double max = a[0][0];
        int maxRow = 0;
        int maxCol = 0;
        
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
               
                if (a[i][j]>max){
                    
                    max = a[i][j];
                    maxRow = i;
                    maxCol = j;
                    
                }
            }
        }
        
        Location location = new Location(); 
        location.row = maxRow;
        location.column = maxCol;
        location.maxValue = max;
        return location;
    }
    
    public static void main(String[] args) {
        double entry = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the numbers of rows and columns of the array:");
        int rows = input.nextInt();
        int cols = input.nextInt();
        double[][] m = new double[rows][cols];
        
        System.out.println("Enter "+rows+"x"+cols+" matrix row by row:");
        
        for (int i = 0; i < m.length; i++) {
            
            for (int j = 0; j < m[i].length; j++) {
                entry = input.nextDouble();
                m[i][j]=entry;
                
            }
            
            input.nextLine();
        }
        System.out.printf(" The maximum number is: %.2f at (%d, %d)", locateLargest(m).maxValue, locateLargest(m).row, locateLargest(m).column);
    }
    
}
