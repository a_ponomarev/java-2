
package smallestelemarray;

import java.util.Scanner;

public class SmallestElemArray {
    public static double min(double[] array) {
        double min = array[0];
        for (int i = 0; i < array.length; i++) {
            double val = array[i];
            if (val<min)
                min=val;
        }
        return min;
        
    }
    
    public static void main(String[] args) {
        System.out.println("Enter 10 nums: ");
        Scanner input = new Scanner(System.in);
        double[] entries = new double[10];
        for (int i = 0; i < 10; i++) {
            entries[i] = input.nextDouble();
        }
        input.nextLine();
        System.out.println("The minimum is: "+min(entries));
    }
    
}
