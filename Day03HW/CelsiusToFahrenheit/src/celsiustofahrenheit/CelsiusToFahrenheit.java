
package celsiustofahrenheit;

public class CelsiusToFahrenheit {

    public static double celsiusToFahrenheit(double celsius){
        return (9.0/5)*celsius+32; 
    }
    
    public static double fahrenheitToCelsius(double fahrenheit){
        return (5.0/9)*(fahrenheit-32);
    }
    
    public static void main(String[] args) {
        System.out.printf("Celsius\t\tFahrenheit\n");
        System.out.printf("--------------------------\n");
        System.out.printf("%5.1f\t\t%6.1f\n", fahrenheitToCelsius(104),celsiusToFahrenheit(40));    
    }
    
}
