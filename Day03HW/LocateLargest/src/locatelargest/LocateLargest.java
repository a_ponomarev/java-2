
package locatelargest;

import java.util.Scanner;

public class LocateLargest {
    
    public static int[] locateLargest(double[][] a)  {
        double max = a[0][0];
        int maxRow = 0;
        int maxCol = 0;
        
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
               
                if (a[i][j]>max){
                    
                    max = a[i][j];
                    maxRow = i;
                    maxCol = j;
                    
                }
            }
        }
        
        int[] arr = {maxRow, maxCol}; 
        
        return arr;
    }

    
    public static void main(String[] args) {
        
        int entry = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the numbers of rows and columns of the array:");
        int rows = input.nextInt();
        int cols = input.nextInt();
        double[][] m = new double[rows][cols];
        
        System.out.println("Enter "+rows+"x"+cols+" matrix row by row:");
        
        for (int i = 0; i < m.length; i++) {
            
            for (int j = 0; j < m[i].length; j++) {
                entry = input.nextInt();
                m[i][j]=entry;
                
            }
            
            input.nextLine();
        }
        System.out.println(" The maximum number is: "+m[locateLargest(m)[0]][locateLargest(m)[1]]);

        
        
        
        
    }
    
}
