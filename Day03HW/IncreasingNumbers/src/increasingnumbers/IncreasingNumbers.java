
package increasingnumbers;

import java.util.Arrays;
import java.util.Scanner;

public class IncreasingNumbers {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] num = new int[3];
        for (int i = 0; i < 3; i++) {
            System.out.println("Please enter the number N"+ i+1);
            num[i]=input.nextInt();
        }
        displaySortedNumbers(num[0],num[1],num[2]);
    }
    
    public static void displaySortedNumbers(double num1, double num2, double num3) {
        double[] arr = new double[3]; 
        arr[0] = num1;
        arr[1] = num2;
        arr[2] = num3;
        
        Arrays.sort(arr);
        
        for (int i = 0; i < arr.length; i++) {
           System.out.print(arr[i] + " "); 
        }
        
    }
    
}
