/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numericalpyramidmethod;

import java.util.Scanner;

/**
 *
 * @author Alex
 */
public class NumericalPyramidMethod {
    
    public static void displayPattern(int size){
        for (int i = 0; i < size; i++) {
            int val = i+1;
            for (int j = size; j >= i; j--) {
                System.out.print(" ");
            }
            
            for (int j=0; j<=i; j++) {
                System.out.print(val);
                val--;   
            }
            

            
            
            
            System.out.println("");     
    }
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = 0;
        String star = "*";
        
        //init
        
        System.out.println("How tall you want the tree to be?");
        size = input.nextInt();
        displayPattern(size);
           
                
             
            
            
            
        }
        
        
    }

