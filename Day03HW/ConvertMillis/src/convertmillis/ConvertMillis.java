
package convertmillis;



public class ConvertMillis {
    
    public static String convertMillis(long millis){
        long hours = millis /1000 / 60 / 60;		
        long minutes = (millis - (hours*1000*60*60)) /1000 / 60 ; 
        long seconds = (millis - ((hours*1000*60*60) + (minutes*1000*60)))/1000; 
        
        String value = hours+":"+minutes+":"+seconds;        
        return value;
    }
    
    public static void main(String[] args) {
        System.out.println(convertMillis(555550000));
    }
    
}
