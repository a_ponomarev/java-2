
package deviation;

import java.util.Scanner;

public class Deviation {

    public static void main(String[] args) {
       double num = 0;
       double[] nList = new double[10];
       double[] sqList = new double[10];
       Scanner input = new Scanner(System.in);
       int i = 0;
       double sum=0;
       double sumSqM=0;
       System.out.println("Please enter 10 digits: ");
       do {
        
           num = input.nextDouble();
           nList[i]=num;
           sum+=nList[i];
           i++;

        } while (num<10);
        for (int j = 0; j < nList.length; j++) {
            sqList[j]= Math.pow(nList[j]-(sum/10), 2);
            sumSqM+=sqList[j];
        }
        
       
        System.out.println("The sum is " + sum);
        System.out.println("The mean is "+sum/10);
        System.out.println("The standard deviation is "+Math.sqrt(sumSqM/9));
    }
    
}
