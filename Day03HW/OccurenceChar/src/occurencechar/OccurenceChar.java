
package occurencechar;

public class OccurenceChar {
    public static int count(String str, char a){
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == a)
                count++;
        }
        return count;
    }
    
    public static void main(String[] args) {
        System.out.println(count("Welcome", 'e'));
    }
    
}
