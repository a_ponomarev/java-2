
package countsingledigits;


public class CountSingleDigits {

    public static void main(String[] args) {
        int[] numList = new int[5];
        int[] counts = new int[10];
        
        for (int i = 0; i < numList.length; i++) {
            int rand = (int)(Math.random()*10);
            numList[i] = rand;
            System.out.print(numList[i]);
        }
        System.out.println();
        System.out.println();
        
        for (int i = 0; i < numList.length; i++) {
            for (int j = 0; j < counts.length; j++) {
                if (j == numList[i]){
                    counts[j]++;
                }
            }
        }
       
        for (int i = 0; i < counts.length; i++) {
            System.out.printf("The digit %d is encountered %d times\n",i,counts[i]);
        }
    }
    
}
