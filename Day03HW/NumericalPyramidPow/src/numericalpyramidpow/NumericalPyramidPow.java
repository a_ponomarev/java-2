/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numericalpyramidpow;

import java.util.Scanner;

/**
 *
 * @author Alex
 */
public class NumericalPyramidPow {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = 0;
        String star = "*";
        
        //init
        
        System.out.println("How tall you want the tree to be?");
        size = input.nextInt();
        
        for (int i = 0; i < size; i++) {
           
            int val=i+1;
            
            for (int j = size; j >= i; j--) {
                System.out.print("    ");
            }
            
            for (int j=0; j<=i; j++) {
                
                if ((int)Math.pow(2, j)>9 && (int)Math.pow(2, j)<99){
                    System.out.print("  "+ (int)Math.pow(2, j));
                }
                else if ((int)Math.pow(2, j)>99){
                    System.out.print(" " + (int)Math.pow(2, j));
                } else
                    System.out.print("   " + (int)Math.pow(2, j));
                
                
                   
            }
            
            for (int j=1; j<=i; j++) {
                
                 if ((int)Math.pow(2, val-2)>9 && (int)Math.pow(2, val-2)<99){
                    System.out.print("  "+ (int)Math.pow(2, val-2));
                }
                else if ((int)Math.pow(2, val-2)>99){
                    System.out.print(" " + (int)Math.pow(2, val-2));
                } else 
                    System.out.print("   " + (int)Math.pow(2, val-2));
                
                val--;
            } 
            
            
            System.out.println("");        
                
             
            
            
            
        }
        
        
    }
    
}
