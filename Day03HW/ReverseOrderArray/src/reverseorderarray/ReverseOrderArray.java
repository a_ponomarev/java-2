
package reverseorderarray;

public class ReverseOrderArray {

    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] revNumbers = new int[numbers.length];
        for (int i = 0, j = numbers.length-1;
                i < numbers.length; i++, j--) {
            
            revNumbers[j] = numbers[i];
            
        }
        for (int i = 0; i < revNumbers.length; i++) {
            System.out.print(revNumbers[i]+" ");
        }

    }
    
}
