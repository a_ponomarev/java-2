
package currenttimemillis;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CurrentTimeMillis {

    public static void main(String[] args) {
        long yourmilliseconds = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");    
        Date resultdate = new Date(yourmilliseconds);
        System.out.println(sdf.format(resultdate));
    }
    
}
