
package assigngrades;

import java.util.Scanner;

public class AssignGrades {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int nbSt = 0;
        int i = 0;
        int grades=0;
        String letterGr = "";
        
        System.out.println("Enter the number of students:");
        nbSt = input.nextInt();
        int[] stGrades = new int[nbSt];
        input.nextLine();
        System.out.println("Enter scores:");
        do {
	   grades = input.nextInt();
	   stGrades[i]=grades;
	   
	   i++;

	} while (i<nbSt);
        
        int max=stGrades[0];
        int val=0;
        
       for (int j = 0; j < stGrades.length; j++) {
            val = stGrades[j];
            
            if (val>max)
                max=val;
        }
        
        for (int j = 0; j < stGrades.length; j++) {
            if (stGrades[j]>=(max-10)){
                letterGr = "A";
            }
            else if (stGrades[j]>=(max-20)){
                letterGr = "B";
            }
            else if (stGrades[j]>=(max-30)){
                letterGr = "C";
            }
            else if (stGrades[j]>=(max-40)){
                letterGr = "D";
            }
            else
                letterGr = "F";
            
            System.out.println("Student "+j+" score  is "+stGrades[j]+" and grade is "+letterGr);
        }
    }
    
}
