
package bookindex;

import java.util.Scanner;

public class BookIndex {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int firstNine = 0;
        int sum = 0;
        System.out.println("Enter the first 9 digits of an ISBN as integer: ");
        firstNine = input.nextInt();
        int copyFirstNine = firstNine;
        int length = String.valueOf(firstNine).length();
        int[] nums = new int[length];
        
        for (int i = 0; i < length; i++) {
            if (i%2==0) {
                nums[i] = (int)(firstNine%10)*(length-i);
                firstNine = firstNine/10;
            }    
            else {
                nums[i] = (int)(firstNine%10)*(length-i);
                firstNine = firstNine/10;
            }
        }
        
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        
        if((sum%11)==10)
            System.out.println("The ISBN-10 number is "+copyFirstNine+"X");
        else
            System.out.println("The ISBN-10 number is "+copyFirstNine+(sum%11));

    }
    
}
