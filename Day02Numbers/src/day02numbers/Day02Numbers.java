package day02numbers;

import java.util.ArrayList;
import java.util.Scanner;

public class Day02Numbers {

    public static void main(String[] args) {
        ArrayList<Integer> numList = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        
        System.out.println("How many numbers do you want to generate? ");
        int count = input.nextInt();
        for (int i=0; i<count; i++) {
            int value = (int)(Math.random()*201-100);
            numList.add(value);
        }
        
        for (int n : numList) {
            if (n <= 0) {
                System.out.println("Less or equal to 0: " + n);
            }
        }
    }
    
}