
package day06enterpeople;
    
import java.util.ArrayList;
import java.util.Scanner;

class Person {

    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void print() {
        System.out.printf("Name: %s; Age: %d\n", name, age);
    }

}

public class Day06EnterPeople {

    
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Person> peopleList = new ArrayList<>();
        float sum=0;
        while (true) {
            System.out.println("Please enter a name, empty to exit: ");
            String name = input.nextLine();
            if (name.equals("")) {
                break;
            }
            System.out.println("Please enter the age: ");
            int age = input.nextInt();
            input.nextLine();
            sum+=age;
            Person p = new Person(name, age);
            peopleList.add(p);
            
         }
  
        // finding the youngest person       
        int youngestIndex = 0;
        
        for (int i = 0; i < peopleList.size(); i++) {
                     
            if (peopleList.get(i).age<peopleList.get(youngestIndex).age){
                youngestIndex = i;
            }
        }
        // finding the shortest name person 
        int shortestIndex = 0;
        
        for (int i = 0; i < peopleList.size(); i++) {
                     
            if (peopleList.get(i).name.length()<peopleList.get(shortestIndex).name.length()){
                shortestIndex = i;
            }
        }
        // matching string
        String str;
        System.out.println("Please enter the search string");
        str = input.nextLine();
        //----------------
        Person youngest = peopleList.get(youngestIndex);
        Person shortestName = peopleList.get(shortestIndex);
        
        System.out.printf("The average is %.2f %n", sum/peopleList.size());
        System.out.printf("The youngest is %s, %d years%n", youngest.name, youngest.age);
        System.out.printf("The person with the shortest name is %s, %d years%n", shortestName.name, shortestName.age);
        
        // matching string continue
        for(int i=0; i<peopleList.size(); i++ ) {
                    
            if (peopleList.get(i).name.contains(str))
                System.out.println("Matching name: " + peopleList.get(i).name + ", " +peopleList.get(i).age);
                
        }
        
        
    }
    
}
