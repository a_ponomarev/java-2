package day04people;

import java.util.Scanner;

public class Day04People {

    public static final int COUNT = 2;
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] namesArray = new String[COUNT];
        int[] agesArray = new int[COUNT];
        
        for (int i = 0; i < namesArray.length; i++) {
            System.out.printf("Enter name of person #%d: ", i+1);
            String name = input.nextLine();
            System.out.printf("Enter age  of person #%d: ", i+1);
            int age = input.nextInt();
            input.nextLine(); // consume the left-over newline character
            namesArray[i] = name;
            agesArray[i] = age;
        }
        
        int youngestIndex = 0;
        for (int i = 0; i < agesArray.length; i++) {
            if (agesArray[i] < agesArray[youngestIndex]) {
                youngestIndex = i;
            }
        }
        System.out.printf("Youngest person is %d and their name is %s\n",
                agesArray[youngestIndex], namesArray[youngestIndex]);
        
    }

}