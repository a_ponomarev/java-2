/* Your program will declare an array of 10 integers.
Generate 10 random integer numbers from -25 to +25 range, both inclusive and place them in the array.

In another loop find and print out, one per line integers that are divisible without remainder both by 3 and 2 but not by 12.

In another loop find the sum and average of all numbers and print both out.

In another loop find the smallest number and print it out.

In another loop find the 2nd smallest number and print it out. */

package quiz1task1;


public class Quiz1Task1 {

    public static void main(String[] args) {
        int[] list = new int[10];
        int rand;
        int sum=0;
        int avg = 0;
        for (int i = 0; i < 10; i++) {
            rand = (int)(Math.random()*51-25);
            list[i] = rand;
            System.out.println(list[i]);
        }
        
        for (int i = 0; i < 10; i++) {
           if (list[i]%2==0 && list[i]%3==0 && list[i]%12!=0)
               System.out.println(list[i]);
        
        }
        
    // average
    
        for (int i = 0; i < list.length; i++) {
                sum+=list[i];

            }
            avg=sum/list.length;
            System.out.println("The average is "+ avg);
        
     // minimum finding
        int smallest = Integer.MAX_VALUE;
        int secondSmallest = Integer.MAX_VALUE;
                      
 
    for (int i = 0; i < list.length; i++) {
        if(list[i]==smallest){
          secondSmallest=smallest;
        } else if (list[i] < smallest) {
            secondSmallest = smallest;
            smallest = list[i];
        } else if (list[i] < secondSmallest) {
            secondSmallest = list[i];
        }

    

            
        }
        System.out.println("The smallest number is "+smallest);
        System.out.println("The 2 smallest number is "+secondSmallest);
    
    }
    
   
    
    
    
}
