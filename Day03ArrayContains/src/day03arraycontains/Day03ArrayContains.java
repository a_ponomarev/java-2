package day03arraycontains;

public class Day03ArrayContains {
    
    public static int[] removeDups(int[] a1, int[] a2) {
        int nonDupCount = a1.length;
        for (int i = 0; i < a1.length; i++) {
            int value = a1[i];
            for (int j = 0; j < a2.length; j++) {
                if (value == a2[j]) {
                    nonDupCount--;
                    break;
                }
            }            
        }
        System.out.println("Non dup count: " + nonDupCount);
        int[] result = new int[nonDupCount];
        // copy the non-dup elements into result array
        int resIdx = 0;
        for (int i = 0; i < a1.length; i++) {
            int value = a1[i];
            boolean isDup = false;
            for (int j = 0; j < a2.length; j++) {
                int cmp = a2[j];
                if (value == cmp) {
                    isDup = true;
                    break;
                }
            }
            // if it is not a duplicate - copy it into resulting array
            if (!isDup) {
                result[resIdx] = value;
                resIdx++;
            }
        }
        return result;
    }
    
    // public static int[] concatenate(int[] a1, int[] a2) { }

    public static void printDups(int[] a1, int a2[]) {
        for (int i = 0; i < a1.length; i++) {
            int value = a1[i];
            int count = 0;
            for (int j = 0; j < a2.length; j++) {
                if (value == a2[j]) {
                    System.out.println(value);
                    break;
                }
            }            
        }
    }

    
    public static void main(String[] args) {
        int [] firstArray = { 1, 3, 7, 8, 2, 7, 9, 11};
        int [] secondArray = { 3, 8, 7, 5, 13, 5, 12, 7 };
        printDups(firstArray, secondArray);
        int [] res = removeDups(firstArray, secondArray);
        System.out.println("Non-dups: ");
        for (int v : res) {
            System.out.print(v + ", ");
        }
        System.out.println("");
    }
    
}