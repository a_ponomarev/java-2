
package day06people;
    
import java.util.ArrayList;

class Person {

    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void print() {
        System.out.printf("Name: %s; Age: %d\n", name, age);
    }

}

public class Day06People {

    public static void main(String[] args) {
        
        ArrayList<Person> people = new ArrayList<>();
        people.add(new Person("Alex", 30));
        people.add(new Person("Mahyar", 35));
        people.add(new Person("Yaroslav", 40));
        
        for (int i = 0; i < people.size(); i++) {
            Person p = people.get(i);
         
            p.print();
            
        }
        
        // finding the oldest person 
        
        int oldestIndex = 0;
        
        for (int i = 0; i < people.size(); i++) {
                        
            if (people.get(oldestIndex).age<people.get(i).age){
                oldestIndex = i;
            }
        }
        
        Person oldest = people.get(oldestIndex);
        
        System.out.printf("The oldest is %s, %d years%n", oldest.name, oldest.age);
        
        
    }
    
}
