/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02namesagain;

import java.util.Scanner;

/**
 *
 * @author 1796188
 */
public class Day02NamesAgain {

    public static void main(String[] args) {
        
        String[] nameList = new String[4];
        Scanner input=new Scanner(System.in);
        String str;
        
        for(int i=0; i<nameList.length; i++){
            System.out.println("Please enter name: ");
            str = input.nextLine();
            if (!(str.equals("")))
                nameList[i]=str;
            else {
                System.out.println("Incorrect value");
                System.exit(0);
            }
            
        }
        
        System.out.println("Please enter the search string");
        str = input.nextLine();
        
        for(int i=0; i<nameList.length; i++ ) {
                    
            if (nameList[i].contains(str))
                System.out.println("Matching name: "+nameList[i]);
        
        }
        int tmpLength=nameList[0].length();
        String tmp="";
        for(int i=0; i<nameList.length; i++ ) {
                  
            if (nameList[i].length()>tmpLength) {
                tmpLength=nameList[i].length();
                tmp=nameList[i];
            } else if (nameList[i].length()==tmpLength) {
                tmp+=(tmp.equals("")?"":"; ") + nameList[i];
            }
                
        
        }
        System.out.println("The longest name: "+tmp);
    }
    
}
