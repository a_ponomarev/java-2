
package day12geoobjfromfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class GeoObj {

    public String getColor() {
        return color;
    }

    public void setColor(String color) throws UnsupportedOperationException {
        if (color.length()<1){
            throw new UnsupportedOperationException("The color name must be at least 1 char");
        }
        this.color = color;
    }
    
    // constructor
    public GeoObj(String color) throws UnsupportedOperationException{
        setColor(color);
    }
    
    private String color;
    
    double getSurface(){
    
        return 0;
    }
    
    double getCircumPerim(){
        
        return 0;
    }
    
    int getVerticeCount(){
        return 0;
    }
    
    int getEdgesCount(){
        return 0;
    }
    
    void print(){
    
    }
}

class Point extends GeoObj {

    public Point(String color) {
        super(color);
    }
    
    @Override
    void print(){
        System.out.printf("Point: Color - %s", this.getColor());
    }
    
}

class Rectangle extends GeoObj {

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) throws UnsupportedOperationException {
        if (width<1){
            throw new UnsupportedOperationException("The width cannot be negative or less than 1 char");
        }
        this.width = width;
    }

    public void setHeight(double height) throws UnsupportedOperationException {
        if (height<1){
            throw new UnsupportedOperationException("The height cannot be negative or less than 1 char");
        }
        
        this.height = height;
    }
    
    // constructor
    
    public Rectangle(String color, double width, double height) throws UnsupportedOperationException{
        super(color);
        setWidth(width);
        setHeight(height);
    }

    private double width;
    private double height;
    
    @Override
    double getSurface(){
        return getWidth()*getHeight();
    }
    
    @Override
    double getCircumPerim(){
        return 2*(getWidth()+getHeight());
    }
    
    @Override
    int getVerticeCount(){
        return 4;
    }
    
    @Override
    int getEdgesCount(){
        return 4;
    }
    
    @Override
    void print(){
        System.out.printf("Rectangle: Color - %s; Surface - %f; Perimeter - %f; Vertices - %d; Edges- %d", this.getColor(), this.getSurface(), this.getCircumPerim(), this.getVerticeCount(), this.getEdgesCount());
    }
    
}
    
class Square extends Rectangle {

    public void setEdgeSize(double edgeSize) throws UnsupportedOperationException {
        if (edgeSize<1){
            throw new UnsupportedOperationException("The value edgeSize cannot be negative or less than 1 char");
        }
        
        this.edgeSize = edgeSize;
    }

    public double getEdgeSize() {
        return edgeSize;
    }

    public Square( String color, double edgeSize) throws UnsupportedOperationException {
        super(color, edgeSize, edgeSize);
        setEdgeSize(edgeSize);
    }

    private double edgeSize;
    
    @Override
    void print(){
        System.out.printf("Square: Color - %s; Surface - %f; Perimeter - %f; Vertices - %d; Edges- %d; Edge size - %f", this.getColor(), this.getSurface(), this.getCircumPerim(), this.getVerticeCount(), this.getEdgesCount(), this.getEdgeSize());
    }
    
}

class Circle extends GeoObj {

    public Circle(String color, double radius) throws UnsupportedOperationException {
        super(color);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) throws UnsupportedOperationException{
        if (radius<1){
            throw new UnsupportedOperationException("The radius cannot be negative or less than 1 char");
        }        
        
        this.radius = radius;
    }
    
    private double radius;
    
    @Override
    double getSurface(){
        return getRadius() * getRadius() * Math.PI;
    }
    
    @Override
    double getCircumPerim(){
        return 2 * Math.PI * getRadius();
    }
    
    @Override
    void print(){
        System.out.printf("Circle: Color - %s, Surface - %f; Perimeter - %f", this.getColor(), this.getSurface(), this.getCircumPerim());
    }
    
}

class Sphere extends GeoObj {
    
    // constructor
    public Sphere(String color, double radius) throws UnsupportedOperationException {
        super(color);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) throws UnsupportedOperationException {
        if (radius<1){
            throw new UnsupportedOperationException("The radius cannot be negative or less than 1 char");
        }           
        this.radius = radius;
    }    
    
    private double radius;
    
    @Override
    double getSurface(){
        return 4 * Math.PI * getRadius() * getRadius();
    }
    
    @Override
    double getCircumPerim(){
        return 2 * Math.PI * getRadius();
    }
    
    @Override
    void print(){
        System.out.printf("Circle: Color - %s, Surface - %f; Perimeter - %f", this.getColor(), this.getSurface(), this.getCircumPerim());
    } 
}

class Hexagon extends GeoObj {

    public int getEdges() {
        return edges;
    }

    public void setEdges(int edges) throws UnsupportedOperationException{
        if (edges<1){
            throw new UnsupportedOperationException("The edges cannot be negative or less than 1 char");
        }         
        
        this.edges = edges;
    }
    
    // constructor
    public Hexagon(String color, int edges) throws UnsupportedOperationException {
        super(color);
        setEdges(edges);
    }

    private int edges;
    
    @Override
    double getSurface(){
        double area = ((3 * (Math.pow(3, 0.5))) / 2) * getEdges() * getEdges();
        area = (area * 10000) / 10000.0;
        return area;
    }
    
    @Override
    double getCircumPerim(){
        return 5*getEdges();
    }
    
    @Override
    int getVerticeCount(){
        return getEdges();
    }
    
    @Override
    int getEdgesCount(){
        return getEdges();
    }
    
    @Override
    void print(){
        System.out.printf("Circle: Color - %s, Surface - %f; Perimeter - %f; Vertices - %d; Edges- %d", this.getColor(), this.getSurface(), this.getCircumPerim(), this.getVerticeCount(), this.getEdgesCount());
    }
    
}

public class Day12GeoObjFromFile {

    static ArrayList<GeoObj> geoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    
    
    public static void main(String[] args) throws FileNotFoundException {
        { // Reading from a text file 
            try (Scanner fileInput = new Scanner(new File("geo.txt"))) {
                
                while (fileInput.hasNextLine()){
                    String line = fileInput.nextLine();
                    String[] data = line.split(";");
                try {
                    switch (data[0]) {
                        case "Point": {
                            if (data.length != 2) {
                                System.out.println("Error creating Point, invalid number of data fields in: " + line);
                                continue;
                            }
                            Point figure = new Point(data[1]);
                            geoList.add(figure);
                            System.out.println("Added: " + line);
                        }
                        break;
                        case "Rectangle": {
                            if (data.length != 4) {
                                System.out.println("Error creating Rectangle, invalid number of data fields in: " + line);
                                continue;
                            }
                            double width = Double.parseDouble(data[2]);
                            double height = Double.parseDouble(data[3]);
                            Rectangle figure = new Rectangle(data[1], width, height);
                            geoList.add(figure);
                            System.out.println("Added: " + line);
                        }
                        break;
                        case "Sphere": {
                            if (data.length != 3) {
                                System.out.println("Error creating Sphere, invalid number of data fields in: " + line);
                                continue;
                            }
                            double radius = Double.parseDouble(data[2]);
                            Sphere figure = new Sphere(data[1], radius);
                            geoList.add(figure);
                            System.out.println("Added: " + line);
                        }
                        break;
                        case "Circle": {
                            if (data.length != 3) {
                                System.out.println("Error creating Circle, invalid number of data fields in: " + line);
                                continue;
                            }
                            double radius = Double.parseDouble(data[2]);
                            Circle figure = new Circle(data[1], radius);
                            geoList.add(figure);
                            System.out.println("Added: " + line);
                        }
                        break;
                        case "Square": {
                            if (data.length != 3) {
                                System.out.println("Error creating Square, invalid number of data fields in: " + line);
                                continue;
                            }
                            double edgeSize = Double.parseDouble(data[2]);
                            Square figure = new Square(data[1], edgeSize);
                            geoList.add(figure);
                            System.out.println("Added: " + line);
                        }
                        break;   
                        case "Hexagon": {
                            if (data.length != 3) {
                                System.out.println("Error creating Hexagon, invalid number of data fields in: " + line);
                                continue;
                            }
                            int edge = Integer.parseInt(data[2]);
                            Hexagon figure = new Hexagon(data[1], edge);
                            geoList.add(figure);
                            System.out.println("Added: " + line);
                        }
                        break;                         
                        default:
                            System.out.println("Error: don't know how to make: " + line);
                            continue;                       
                        }
                    } catch (UnsupportedOperationException ex){
                        System.out.println("Invalid data " + ex.getMessage() +" in the line: "+ line);
                    } catch (java.util.InputMismatchException ex){
                        System.out.println("Invalid data " + ex.getMessage() +" in the line: "+ line);
                    } catch (java.lang.NumberFormatException ex){
                        System.out.println("Invalid data " + ex.getMessage() +" in the line: "+ line);
                    }
                                
                }
            } catch (IOException ex){
                System.out.println("File reading error: "+ex.getMessage());
            }
            
            System.out.println("\n***** Enlisting all the items added *****\n");
            
            for (GeoObj geo: geoList) {
                
                geo.print();
                System.out.println();
            }
        } 
    }
    
}
