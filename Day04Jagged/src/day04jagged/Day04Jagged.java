
package day04jagged;


public class Day04Jagged {


    public static void main(String[] args) {
        int [][] twoJagged = {
            {1,2,3},
            {4,5},
            {6,7,8,9,10}
        };
        
        for (int row = 0; row < twoJagged.length; row++) {
            
            for (int col = 0; col < twoJagged[row].length; col++) {
                
                System.out.print( ( (col==0)?"":"," ) + twoJagged[row][col]);
                
            }
            System.out.println();
        }
        
    }
    
}
