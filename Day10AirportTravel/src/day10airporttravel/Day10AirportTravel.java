
package day10airporttravel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

class ParameterInvalidException extends Exception {
    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Airport {
    
    private String code;
    private String city;
    private double latitude;
    private double longitude;
    
    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setCode(String code) throws ParameterInvalidException {
        boolean hasUppercase = !code.equals(code.toLowerCase());
        boolean is3 = code.length() == 3;
        
        if (!(hasUppercase && is3)){
            throw new ParameterInvalidException("The airport code should be 3 UpperCase letters");
        }
        this.code = code;
    }

    public void setCity(String city) throws ParameterInvalidException {
        if (city == null || city.equals("")){ // порядок важен: потому что даст NullPointerException если equals будет на 1 месте - будет ссылаться на объект, который не существует.
            throw new ParameterInvalidException("City name shoudn't be blank");
        }
        
        this.city = city;
    }

    public void setLatitude(double latitude) throws ParameterInvalidException {
        if (latitude<-90 || latitude>90){
            throw new ParameterInvalidException("Latitude should be within -90 and 90 range");
        }
        
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) throws ParameterInvalidException {
        if (longitude<-180 || longitude>180){
            throw new ParameterInvalidException("Longitude should be within -180 and 180 range");
        }
        
        this.longitude = longitude;
    }

    public Airport(String code, String city, double latitude, double longitude) throws ParameterInvalidException {
        setCode(code);
        setCity(city);
        setLatitude(latitude);
        setLongitude(longitude);
    }
     
    public void print(){
        System.out.printf("Code: %s; City: %s; Latitude: %f; Longitude: %f%n",this.code,this.city, this.latitude, this.longitude);
    } 
}

public class Day10AirportTravel {

    static ArrayList<Airport> airportList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static int inputValue = 0;

    public static void userMenu(){
     
            System.out.println();
            System.out.println("1. Show all airports (codes and city names)");
            System.out.println("2. Find distance between two airports by codes.");
            System.out.println("3. Find the 1 airport nearest to an airport given and display the distance.");
            System.out.println("4. Add a new airport to the list.");
            System.out.println("0. Exit.");
            inputValue = input.nextInt();
            input.nextLine();
      
    }
    
    public static void printAirports() {
            for ( Airport airport: airportList) {
                airport.print();
            }
    }
    
    public static double distance(double lat1, double lat2, double lon1,
        double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
    
    public static void getDistanceByCode(){
        System.out.println("Please enter the airport #1 code:");
        String askCode1 = input.nextLine().toUpperCase();
        System.out.println("Please enter the airport #2 code:");
        String askCode2 = input.nextLine().toUpperCase();        
        Airport[] arrForComp = new Airport[2];
        for (int j = 0; j < airportList.size(); j++) {
            if (airportList.get(j).getCode().contains(askCode1)){
                arrForComp[0] = airportList.get(j);
            }
        }
        for (int i = 0; i < airportList.size(); i++) {
            if (airportList.get(i).getCode().contains(askCode2)){
                arrForComp[1] = airportList.get(i);
            }
        }
        System.out.printf("The distance is %.2f Kilometers\n", distance(arrForComp[0].getLatitude(),arrForComp[1].getLatitude(),arrForComp[0].getLongitude(),arrForComp[1].getLongitude(),0,0)/1000);
        
    }
    
    public static void addNew() throws ParameterInvalidException{
    { // Writing to a file
            System.out.println("Enter the airport code: ");
            String line = input.nextLine();
            System.out.println("Enter the airport city: ");
            line += ";"+input.nextLine();
            System.out.println("Enter the airport latitude: ");
            line += ";"+input.nextLine();
            System.out.println("Enter the airport longitude: ");
            line += ";"+input.nextLine()+"\n";
            
            String array1[]= line.split(";");
            Airport aP = new Airport(array1[0], array1[1], Double.parseDouble(array1[2]), Double.parseDouble(array1[3]));
            airportList.add(aP);
            
            try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("airports.txt", true)))){
              pw.append(line);

              
            } catch (IOException ex) {
                System.out.println("File writing error: " + ex.getMessage());
            }
        }
    }
    
    public static void getNearest(){
    
        System.out.println("Please enter the airport code:");
        String askCode1 = input.nextLine().toUpperCase();
        int index = 0;
       
        for (int j = 0; j < airportList.size(); j++) {
            if (airportList.get(j).getCode().contains(askCode1)){
                index = j;
            }
        }
        
        double max = Double.MAX_VALUE;
        int nearestIndex=0;
        for (int i = 0; i < airportList.size(); i++) {
                double val = distance(airportList.get(index).getLatitude(),airportList.get(i).getLatitude(),airportList.get(index).getLongitude(),airportList.get(i).getLongitude(),0,0)/1000;
                
                if (val < max && val != 0 && val != max){
                    max = val;
                    nearestIndex = i;
                }
                
        }
        
        System.out.println("The nearest is ");
        airportList.get(nearestIndex).print();
        
        

        
        
        
        
    
    }
    
    public static void main(String[] args) throws ParameterInvalidException {
        { // Reading from a text file 
            try (Scanner fileInput = new Scanner(new File("airports.txt"))) {
                
                
                while (fileInput.hasNextLine()){

                    try {
                        String line = fileInput.nextLine();
                        String array1[]= line.split(";");
                        Airport aP = new Airport(array1[0], array1[1], Double.parseDouble(array1[2]), Double.parseDouble(array1[3]));
                        airportList.add(aP);
                    } catch (ParameterInvalidException ex){
                        System.out.println("Invalid data" + ex.getMessage());
                    } catch (java.util.InputMismatchException ex){
                        System.out.println("Invalid data" + ex.getMessage());
                    }
                }
                
            } catch (IOException ex){
                System.out.println("File reading error: "+ex.getMessage());
            } 
        }
        
        { // user menu + switch 
            while (true) {
                userMenu();
                
                switch (inputValue){
                    case 1:
                        printAirports();
                        break;
                    case 2:
                        getDistanceByCode();
                        break;
                    case 3:
                        getNearest();
                        break;
                    case 4:
                        addNew();
                        break;
                    case 0:
                        System.exit(0);
                    default:
                        System.out.println("This shouldn't happen");
                }
                
            }
        }
    }
    
}
