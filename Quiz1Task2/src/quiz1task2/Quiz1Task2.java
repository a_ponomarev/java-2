/*
 Declare an array of 5 Strings.
Declare an array of 5 floating point values.

Ask user for 5 names of cities and their populations in millions.
Ask for one city name, then population, on separate lines, then repeat for next city.

Ask user for one letter (string with length one).
Find all cities whose names contain that letter and print out their names and populations (one name and population per line).

Find the city of the largest population and print out its name and population.
 */
package quiz1task2;

import java.util.Arrays;
import java.util.Scanner;


public class Quiz1Task2 {

   
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] strList = new String[5];
        float[] flList = new float[5];
        int i = 0;
        String sStr = "";
        
        while (i<strList.length){
            System.out.println("Enter city name");
            strList[i]=input.nextLine();
            System.out.println("Enter city's popualtion: ");
            flList[i]=input.nextFloat();
            input.nextLine();
            i++;
        }
        while(true){
        System.out.println("Please enter the search string ");
        sStr=input.nextLine();
        if (sStr.length()==1){
            break;
        }
        }
        // showing all the cities which contain the string
         for (int j = 0; j < strList.length; j++) {
            if (strList[j].contains(sStr))
                System.out.println("Matching city: "+strList[j]+" and it's population is "+flList[j] + " people");
        }
         
         //max population city
         float max = flList[0];
         float indx = 0;
         for (int j = 0; j < flList.length; j++) {
            float n = flList[j];
            

            if (n > max) {
                max = n;
                indx = Arrays.asList(flList).indexOf(j);
        }
            
             
       
    }
         System.out.println("The highest populated city is "+ strList[(int)indx]+" with the population of " + flList[(int)indx]);
    }
}
