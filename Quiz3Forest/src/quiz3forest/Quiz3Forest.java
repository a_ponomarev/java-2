package quiz3forest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

class InvalidDataException extends Exception {

    public InvalidDataException(String message) {
        super(message);
    }
}

class ForestItem {

    public static ForestItem createFromDataString(String[] data, String line) throws InvalidDataException {
        // Factory method - this is where your split and switch/case goes
        // returns the correct object instantiated
        // Note: ForestItem, Animal, Green and Inanimate can NOT be instantiated.
        switch (data[0]) {
                        case "Bear": {
                            if (data.length != 5) {
                                System.out.println("Error creating Point, invalid number of data fields in: " + line);
                                
                            }
                            double age = Double.parseDouble(data[2]);
                            double tall = Double.parseDouble(data[3]);
                            double weight = Double.parseDouble(data[4]);
                            return new Bear(data[1],age,tall,weight);
                        
                        }
                        
                        case "Wolf": {
                            if (data.length != 5) {
                                System.out.println("Error creating Rectangle, invalid number of data fields in: " + line);
                                
                            }
                            double age = Double.parseDouble(data[2]);
                            double tall = Double.parseDouble(data[3]);
                            double weight = Double.parseDouble(data[4]);
                            return new Wolf(data[1],age,tall,weight);

                        }
                       
                        case "Bush": {
                            if (data.length != 5) {
                                System.out.println("Error creating Sphere, invalid number of data fields in: " + line);
                                
                            }
                            double age = Double.parseDouble(data[2]);
                            double tall = Double.parseDouble(data[3]);
                            double weight = Double.parseDouble(data[4]);
                            return new Bush(data[1],age,tall,weight);
                        }
                       
                        case "Tree": {
                            if (data.length != 5) {
                                System.out.println("Error creating Circle, invalid number of data fields in: " + line);
                                
                            }
                            double age = Double.parseDouble(data[2]);
                            double tall = Double.parseDouble(data[3]);
                            double weight = Double.parseDouble(data[4]);
                            return new Tree(data[1],age,tall,weight);
                        }
                        
                        case "Rock": {
                            if (data.length != 5) {
                                System.out.println("Error creating Square, invalid number of data fields in: " + line);
                                
                            }
                            double age = Double.parseDouble(data[2]);
                            double tall = Double.parseDouble(data[3]);
                            double weight = Double.parseDouble(data[4]);
                            return new Rock(data[1],age,tall,weight);
                        }
                          
                        case "Waterfall": {
                            if (data.length != 5) {
                                System.out.println("Error creating Hexagon, invalid number of data fields in: " + line);
                                
                            }
                            double age = Double.parseDouble(data[2]);
                            double tall = Double.parseDouble(data[3]);
                            double weight = Double.parseDouble(data[4]);
                            return new Waterfall(data[1],age,tall,weight);
                        }
                                                 
                        default:
                           System.out.println("Error: don't know how to make: " + line);                           break;
                             
        
    }
        throw new InvalidDataException("Cannot create an entry "+line);
    }

    public String saveToDataString() throws InvalidDataException {
        return String.format("%s-%s-%.2f-%.2f-%.2f",
                this.getClass().getSimpleName(), getName(), getAge(), getHeight(), getWeight());
    }
    
    @Override
    public String toString() {
        return String.format("%s (named %s) is %.2f y/o, %.2f tall and weighs %.2f kg",
                this.getClass().getSimpleName(), getName(), getAge(), getHeight(), getWeight());
    }
  
    public ForestItem(String name, double age, double height, double weight) throws InvalidDataException {
        setName(name);
        setAge(age);
        setHeight(height);
        setWeight(weight);
    }
    
    private String name;
    private double age;
    private double height;
    private double weight;

    public String getName() {
        return name;
    }

    public double getAge() {
        return age;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    public void setName(String name) throws InvalidDataException {
        if (name.equals("")){
            throw new InvalidDataException("The name shouldn't be blank");
        }
        this.name = name;
    }

    public void setAge(double age) throws InvalidDataException {
        if (age<0){
            throw new InvalidDataException("Age shouldn't be negative");
        }
        
        this.age = age;
    }

    public void setHeight(double height) throws InvalidDataException {
        if (height<0){
            throw new InvalidDataException("Height shouldn't be negative");
        }
        this.height = height;
    }

    public void setWeight(double weight) throws InvalidDataException {
        if (weight<0){
            throw new InvalidDataException("Weight shouldn't be negative");
        }
        this.weight = weight;
    }
}


class Animal extends ForestItem{
    
    private static int animalCount;
    
    public Animal(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
        animalCount++;
    }

    public static int getAnimalCount() {
        return animalCount;
    }

     
}

class Green extends ForestItem{
    
    private static int greenCount;
    
    public Green(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
        greenCount++;
    }

    public static int getGreenCount() {
        return greenCount;
    }
    
}

class Inanimate extends ForestItem{
    
    private static int inanimateCount;
    
    public Inanimate(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
        inanimateCount++;
    }

    public static int getInanimateCount() {
        return inanimateCount;
    }
    
}

class Bear extends Animal{

    public Bear(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
    }
    
}

class Wolf extends Animal{

    public Wolf(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
    }
    
}

class Bush extends Green{

    public Bush(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
    }
    
}

class Tree extends Green{

    public Tree(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
    }
    
}

class Rock extends Inanimate{

    public Rock(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
    }
    
}

class Waterfall extends Inanimate{

    public Waterfall(String name, double age, double height, double weight) throws InvalidDataException {
        super(name, age, height, weight);
    }
    
}



public class Quiz3Forest {

    private static ArrayList<ForestItem> forestList = new ArrayList<>();

    public static void main(String[] args) throws InvalidDataException {

        
        { // Reading from a text file 
            try (Scanner fileInput = new Scanner(new File("input.txt"))) {
                
                while (fileInput.hasNextLine()){
                    String line = fileInput.nextLine();
                    String[] data = line.split(";");
                try {
                     
                    forestList.add(ForestItem.createFromDataString(data, line));
                    System.out.println("Added: " + line);
                     
                    } catch (InvalidDataException ex){
                        System.out.println("Invalid data " + ex.getMessage() +" in the line: "+ line);
                    } catch (java.util.InputMismatchException ex){
                        System.out.println("Invalid data " + ex.getMessage() +" in the line: "+ line);
                    } catch (java.lang.NumberFormatException ex){
                        System.out.println("Invalid data " + ex.getMessage() +" in the line: "+ line);
                    }
                                
                }
            } catch (IOException ex){
                System.out.println("File reading error: "+ex.getMessage());
            }
            
            System.out.println("\n***** Enlisting all the items added *****\n");

            
            for (ForestItem fI: forestList) {
                System.out.println(fI);
            }
        }
        
        double maxAge = forestList.get(0).getAge();
        int maxAgeIndex = 0;
        for (int i = 0; i < forestList.size(); i++) {
          
            double val = forestList.get(i).getAge();
            if (val>maxAge){
                
                maxAge = val;
                maxAgeIndex = i;
                
            }

                
        }
        
        System.out.println();
        
        System.out.println("The oldest is "+forestList.get(maxAgeIndex));
        
        double maxHeight = forestList.get(0).getHeight();
        int maxHeightIndex = 0;
        for (int i = 0; i < forestList.size(); i++) {
            
            double val = forestList.get(i).getHeight();
            
            if (val>maxHeight){
                
                maxHeight = val;
                maxHeightIndex = i;
                
            }
  
                
        }
        System.out.println("The tallest is "+forestList.get(maxHeightIndex));
        
        double maxWeight = forestList.get(0).getWeight();
        int maxWeightIndex = 0;
        for (int i = 0; i < forestList.size(); i++) {
            
            double val = forestList.get(i).getWeight();
            if (val>maxWeight){
                
                maxWeight = val;
                maxWeightIndex = i;
                
            }
 
                
        }
        System.out.println("The heaviest is "+forestList.get(maxWeightIndex));
        
        System.out.println();
        
        System.out.println("Animals - "+ Animal.getAnimalCount());
        System.out.println("Green - "+ Green.getGreenCount());
        System.out.println("Inanimate - "+ Inanimate.getInanimateCount());

        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("maximums.txt")))) {
            
                String data = forestList.get(maxAgeIndex).saveToDataString();
                pw.println(data);
                data = forestList.get(maxHeightIndex).saveToDataString();
                pw.println(data);
                data = forestList.get(maxWeightIndex).saveToDataString();
                pw.println(data);
                
        } catch (IOException ex) {
            System.out.println("File writing error: " + ex.getMessage());
        }
    }

}