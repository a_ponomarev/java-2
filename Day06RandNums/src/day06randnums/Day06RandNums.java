
package day06randnums;

import java.util.ArrayList;

class RandomStore {

    int nextInt(int minIncl, int maxExcl) {
        int rand = (int) (Math.random()*(maxExcl-minIncl)+minIncl);
        
        intHistory.add(rand);
                  
        return rand;
    }
    
    ArrayList<Integer> intHistory = new ArrayList<>();
    
    void printHistory() {
        for (int p: intHistory) {
            System.out.print(p +"; ");
        }
    }
    
}
public class Day06RandNums {

    public static void main(String[] args) {
       RandomStore rs = new RandomStore();
	int v1 = rs.nextInt(1, 10);
	int v2 = rs.nextInt(-100, -10);
	int v3 = rs.nextInt(-20,21);
	System.out.printf("v1=%d, v2=%d, v3=%d\n", v1, v2, v3);
        rs.printHistory();
    }
    
}
