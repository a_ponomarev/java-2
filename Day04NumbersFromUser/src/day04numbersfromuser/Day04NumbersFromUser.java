
package day04numbersfromuser;

import java.util.Scanner;


public class Day04NumbersFromUser {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float num = 0;
        int count = 0;
        double sum = 0;
        
        while (true) {
            System.out.println("Enter floating number: ");
            num = input.nextFloat();
        
            if (num==0)
                break;
            else {
                sum+=num;
                count++;
            }
            

        }  
        
            System.out.println("You've entered " + count+" entries");
            System.out.printf("The sum of the numbers is %.1f%n", sum);
            System.out.printf("The average is %.2f%n", sum/count);
    }
    
}
