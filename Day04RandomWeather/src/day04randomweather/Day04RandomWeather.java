/*
Range is -50 to +50 (inclusive)

Q1: How many integer numbers in this range? 101
Then multiply result of Math.random() by this number, this gives us:

Math.random()*101 gives values from 0.0000 to 100.9999999999999999999

Q2: If Math.random() returns a 0, what do you need to add/subtract to get the lowest number from your range? -50

Math.random()*101-50 give values from -50.0000000 to 50.9999999999999999
 */
package day04randomweather;

public class Day04RandomWeather {

    public static void main(String[] args) {
       int randInt = (int)(Math.random()*61-30);
        System.out.println(randInt);
        if (randInt<=-15) 
            System.out.println("very very cold");
        else if (randInt < 0)
            System.out.println("Freezing");
        else if (randInt < 15 )
            System.out.println("Spring or Fall");
        else 
            System.out.println("That's what I like");
       
          
    }
    
}
