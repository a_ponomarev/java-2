/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02arrays;


import java.util.Scanner;

/**
 *
 * @author 1796188
 */
public class Day02Arrays {

    public static void main(String[] args) {
            
            
            Scanner input = new Scanner(System.in);
            int tall;
            String star="*";
            
            System.out.println("How big the tree should be?");
            tall=input.nextInt();
            String[] tree = new String[tall];
            int width=60;
                          
            for(int i=0; i<tree.length; i++) {
                System.out.printf("%"+(width+i)+"s%n", star);
                star+="**";
            }
            

            
    }
    
}
