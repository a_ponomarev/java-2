
package day10sureinput;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Day10SureInput {
    static Scanner input = new Scanner(System.in);
    
    public static int inputInt() {
        while (true){ 
            try {
                int val = input.nextInt();
                return val;
            } catch (java.util.InputMismatchException ex){
                input.nextLine();
                System.out.println("input error try again");
            }
        }
        
    }

    public static void main(String[] args) {
        
       System.out.println("Enter an integer value: ");
       int val  = inputInt();
       System.out.println("You've entered " + val);
      
    }
    
}
