
package day08todosmenumethods;

import java.util.ArrayList;
import java.util.Scanner;

class Todo {

    public Todo(String task, String dueDate, int hoursOfWork) throws Exception {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) throws Exception {
        if (task.length()<2 || task.length() >50)
            throw new Exception("Task should be between 2 and 50 chars");
        
        this.task = task;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) throws Exception {
        if (dueDate.length()<2 || dueDate.length() >20)
            throw new Exception("Due date should be between 2 and 20 chars");
        this.dueDate = dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) throws Exception {
        if (hoursOfWork<=0)
            throw new Exception("Hours of work should be more than zero");
        this.hoursOfWork = hoursOfWork;
    }
    private String task; // 2-50 chars
    private String dueDate; // 6-20 chars
    private int hoursOfWork; // 0 >
    
    public void print(){
        System.out.println("Todo: "+ this.task+", "+this.dueDate+", will take "+this.hoursOfWork+" hour(s) of work");
    }
}

public class Day08TodosMenuMethods {
    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static int inputValue = 0;
    static String task, dueDate;
    static int hoursOfWork;
    
    public static void userMenu(){
        
            System.out.println();
            System.out.println("1. Add a todo");
            System.out.println("2. List all todos (numbered)");
            System.out.println("3. Delete a todo");
            System.out.println("4. Modify a todo");
            System.out.println("0. Exit");
            System.out.print("Your pick is: ");
            inputValue = input.nextInt();
            input.nextLine();
            
    
    }
    
    public static void addTodo(){
    
        System.out.println("Enter task description: ");
        task = input.nextLine();
        System.out.println("Enter due date: ");
        dueDate = input.nextLine();
        System.out.println("Enter hours of work");
        hoursOfWork = input.nextInt();
    
    }
    
    
    
    public static void main(String[] args) {
        ArrayList<Todo> todoList = new ArrayList<>();
        
        Scanner input = new Scanner(System.in);
        
        while (true) {
            
            userMenu();
            
            switch (inputValue){
                
                case 1:
                    
                    addTodo();
                    try {
                        Todo nTask = new Todo(task, dueDate, hoursOfWork);
                        todoList.add(nTask);
                        System.out.println("You've created following todo: "); 
                        nTask.print();
                    } catch (Exception ex){
                            System.out.println
                                ("Error while creating Todo: "+ex.getMessage());
                        }
                    break;
                
                case 2:
                    
                    for (int i = 0; i < todoList.size(); i++) {
                        System.out.print(i+1+". ");
                        todoList.get(i).print();
                    }
                    break;
                
                case 3:
                     
                    System.out.println("Which todo you would like to delete?");
                    int delEntry = input.nextInt();
                    if (todoList.size()>0){
                        try {
                            todoList.remove(delEntry-1);
                            System.out.println("The entry "+delEntry+" is deleted");
                        } catch (Exception ex){
                            System.out.println("No such entry!");
                        }
                            
                    }
                    else
                        System.out.println("There are no todos");
                    
                    break;
                    
                case 4:
                     
                    System.out.println("Which todo you would like to modify?");
                    int modEntry = input.nextInt();
                    System.out.println("Modyfying the following entry:");
                    todoList.get(modEntry-1).print();
                    input.nextLine();
                    addTodo();
                    try {
                        todoList.remove(modEntry-1);
                        Todo newTask = new Todo(task, dueDate, hoursOfWork);
                        todoList.add(modEntry-1,newTask);
                        System.out.println("You've modified the todo #"+modEntry+":"); 
                        newTask.print();
                    } catch (Exception ex){
                            System.out.println
                                ("Error while modifying Todo: "+ex.getMessage());
                        }
                    break;
                    
                case 0:
                    
                    System.out.println("Exiting. Good bye!");
                    System.exit(1);
                    
                default:
                    
                    System.out.println("Please enter from 0-4");
                    
            }
            
        }
        
       
        
        
    }
    
}
