
package day08peopleverified;

class Person {

    public Person(String name, int age) throws Exception {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        if (name.length()< 2)
            throw new Exception("Name must be at least 2 ch long");
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if (age < 0 || age > 150)
            throw new Exception("Age must be between 0 and 150");
        this.age = age;
    }
    private String name;
    private int age;
}

public class Day08PeopleVerified {

    public static void main(String[] args) {
        try {
            Person p = new Person("J", -33);
            p.setName("M");
            p.setAge(-1);
        } catch (Exception ex) {
            System.out.println("Error occured: "+ ex.getMessage());
        }
    }
    
}
