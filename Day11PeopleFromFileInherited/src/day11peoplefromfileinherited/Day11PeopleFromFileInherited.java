
package day11peoplefromfileinherited;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class ParameterInvalidException extends Exception {
    public ParameterInvalidException(String message) {
        super(message);
    }
}
// Person
class Person {

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) throws ParameterInvalidException {
        if (!(name.matches("^[a-zA-Z]{2,}$"))){
            throw new ParameterInvalidException("The name must be at least 2 ch long");
        }
            
        
        this.name = name;
    }

    public void setAge(int age) throws ParameterInvalidException {
        if (age<0 || age>150){
           throw new ParameterInvalidException("The age must be between 0 and 150");
        }
        
        this.age = age;
    }
    
    private String name;
    private int age;

    public Person(String name, int age) throws ParameterInvalidException {
        setName(name);
        setAge(age);
    }
    
    public String toString(){
        return String.format("Person: %s, %d", getName(), getAge());
    }
}

// Teacher
class Teacher extends Person {
    
    private String discipline;
    private int yearsOfExp;

    public Teacher(String name, int age, String discipline, int yearsOfExp) throws ParameterInvalidException {
        super(name, age);
        setDiscipline(discipline);
        setYearsOfExp(yearsOfExp);
    }
    
    public String toString(){
        return String.format("Teacher: %s, %d, %s, %d", getName(), getAge(), getDiscipline(), getYearsOfExp());
    }

    public String getDiscipline() {
        return discipline;
    }

    public int getYearsOfExp() {
        return yearsOfExp;
    }

    public void setDiscipline(String discipline) throws ParameterInvalidException {
        if (!(discipline.matches("^[a-zA-Z]{2,}$"))){
            throw new ParameterInvalidException("Discipline should be at least 2 characters");
        }
        
        this.discipline = discipline;
    }

    public void setYearsOfExp(int yearsOfExp) {
        this.yearsOfExp = yearsOfExp;
    }
}

// Student
class Student extends Person {

    public String getProgram() {
        return program;
    }

    public Double getGpa() {
        return gpa;
    }

    public void setProgram(String program) throws ParameterInvalidException {
        if (!(program.matches("^[a-zA-Z]{2,}$"))){
            throw new ParameterInvalidException("The program must be at least 2 ch long");
        }
        
        this.program = program;
    }

    public void setGpa(Double gpa) throws ParameterInvalidException {
        if (gpa<0 || gpa>4.3 ){
            throw new ParameterInvalidException("GPA should be from 0 to 4.3");
        }
        
        this.gpa = gpa;
    }
    private String program;
    private Double gpa;

    public Student(String name, int age, String program, Double gpa) throws ParameterInvalidException {
        super(name, age);
        this.program = program;
        this.gpa = gpa;
    }
    
    public String toString(){
        return String.format("Student: %s, %d, %s, %f", getName(), getAge(), getProgram(), getGpa());
    }
}

public class Day11PeopleFromFileInherited {
    
    static ArrayList<Person> peopleList = new ArrayList<>();
    static String line;
    static int youngest = Integer.MAX_VALUE;
    static int yIndex = 0;
    
    public static void findingYoungest(){
        for (int i = 0; i < peopleList.size(); i++) {
            int val = peopleList.get(i).getAge();
            
            if (val<youngest){
                yIndex = i;
            }
        }
        System.out.println("Youngest is "+ peopleList.get(yIndex).toString());
    }
    
    public static void main(String[] args) {
        { // Reading from a text file 
            try (Scanner fileInput = new Scanner(new File("peopletypes.txt"))) {
                
                
                while (fileInput.hasNextLine()){

                    try {
                        Person nP = null;
                        line = fileInput.nextLine();
                        String array1[]= line.split(";");
                        if (array1[0].equals("Person")){
                            if (array1.length==3){
                                nP = new Person(array1[1], Integer.parseInt(array1[2]));
                            }
                        } 
                        else if (array1[0].equals("Student")){
                            if (array1.length==5){
                                nP = new Student(array1[1], Integer.parseInt(array1[2]),                                    array1[3], Double.parseDouble(array1[4]));
                            }
                        }
                        else if (array1[0].equals("Teacher")){
                            if (array1.length==5){
                                nP = new Teacher(array1[1], Integer.parseInt(array1[2]),                                    array1[3], Integer.parseInt(array1[4]));
                            }
                        }

                        if (nP != null){
                            peopleList.add(nP);
                        }
                        else {
                            System.out.println(line+" - doesn't contain valid parameters");
                        }
                    } catch (ParameterInvalidException ex){
                        System.out.println("Invalid data " + ex.getMessage() +" in the line: "+ line);
                    } catch (java.util.InputMismatchException ex){
                        System.out.println("Invalid data" + ex.getMessage());
                    }
                }
                
            } catch (IOException ex){
                System.out.println("File reading error: "+ex.getMessage());
            }
            for (Person p: peopleList) {
                System.out.println(p);
            }
            
            findingYoungest();
        }        
    }
    
}
