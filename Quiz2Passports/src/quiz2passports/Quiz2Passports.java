
package quiz2passports;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

class InvalidValueException extends Exception {
    InvalidValueException(String message) {
        super(message);
    }
}

class Passport {

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) throws InvalidValueException {
        if (!(number.matches("[A-Z]{2}+\\d{6}+")))
            throw new InvalidValueException("The format should be like this: AB123456");
        this.number = number;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) throws InvalidValueException {
        if (firstName.length()<2)
            throw new InvalidValueException("First Name should be at least 2 characters");
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws InvalidValueException {
        if (lastName.length()<2)
            throw new InvalidValueException("Last Name should be at least 2 characters");
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) throws InvalidValueException {
        if (address.length()<2)
            throw new InvalidValueException("Address should be at least 2 characters");
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws InvalidValueException {
        if (city.length()<2)
            throw new InvalidValueException("City Name should be at least 2 characters");
        this.city = city;
    }

    public double getHeightCm() {
        return heightCm;
    }

    public void setHeightCm(double heightCm) throws InvalidValueException {
        if (heightCm<0 || heightCm >300)
            throw new InvalidValueException("The range for height in cm is from 0 to 300 cm");
        
        this.heightCm = heightCm;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(double weightKg) throws InvalidValueException {
        if (weightKg<0 || weightKg >300)
            throw new InvalidValueException("The range for weight in kg is from 0 to 300 kg");
        this.weightKg = weightKg;
    }

    public int getYob() {
        return yob;
    }

    public void setYob(int yob) throws InvalidValueException {
        if (yob<1900 || yob >2050)
            throw new InvalidValueException("The year of birth range is from 1900 to 2050");
        
        this.yob = yob;
    }

    public Passport(String number, String firstName, String lastName, String address, String city, double heightCm, double weightKg, int yob) throws InvalidValueException {
        setNumber(number);
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setCity(city);
        setHeightCm(heightCm);
        setWeightKg(weightKg);
        setYob(yob);
    }
    
    public void print(){
        System.out.printf(
        "Passenger: %n 1. Passport number: %s; 2. First name: %s; 3. Last Name: %s; 4. Address: %s; 5. City: %s; 6. Height in cm: %.2f; 7. Weight in kg: %.2f; 8. YOB: %d;%n", number, firstName, lastName, address, city, heightCm, weightKg, yob);
    }
    
private String number; // passport number AB123456 format exactly    
private String firstName, lastName, address, city; // at least 2 letters each
private double heightCm, weightKg; // height from 0-300, weight 0-300
private int yob; // year of birth - between 1900-2050
}

public class Quiz2Passports {
  
  static int inputValue = 0;
  static Scanner input = new Scanner(System.in);  
  static ArrayList<Passport> passList = new ArrayList<>();
  static String number, firstName, lastName, address, city;
  static double heightCm, weightKg;
  static int yob;
  
    public static void addPassenger(){
    
        System.out.println("Enter passport number: ");
        number = input.nextLine();
        System.out.println("Enter first name: ");
        firstName = input.nextLine();
        System.out.println("Enter last name:");
        lastName = input.nextLine();
        System.out.println("Enter address:");
        address = input.nextLine();
        System.out.println("Enter city:");
        city = input.nextLine();
        System.out.println("Enter height:");
        heightCm = input.nextDouble();
        System.out.println("Enter weight:");
        weightKg = input.nextDouble();  
        System.out.println("Enter YOB:");
        yob = input.nextInt(); 
    }
    
    public static void userMenu(){
        
            System.out.println();
            System.out.println("1. Display all passports data (one per line)");
            System.out.println("2. Display all passports for people in the same city (ask for city name)");
            System.out.println("3. Find the tallest person and display their info");
            System.out.println("4. Find the lightest person and display their info");
            System.out.println("5. Display all people younger than certain age (ask for max age, not year)");
            System.out.println("6. Add person/passport to list");
            System.out.println("0. Exit");
            System.out.print("Your pick is: ");
            inputValue = input.nextInt();
            input.nextLine();
            System.out.println();
            
    
    }
        
    public static void getTallest(){
        double max = passList.get(0).getHeightCm();
        int tallestIndex = 0;

        for (int i = 0; i < passList.size(); i++) {
            double val = passList.get(i).getHeightCm();

            if (val>max){
                max = val;
                tallestIndex = i;
            }

        }

        System.out.println("The tallest is: ");
        passList.get(tallestIndex).print();
    }
    
    public static void sameCityPassengers() {
        System.out.println("What city you want the passenger information from:");
        String askCity = input.nextLine();
        System.out.println("Matching passengers: ");
        for (int j = 0; j < passList.size(); j++) {
            if (passList.get(j).getCity().contains(askCity))
                passList.get(j).print();
        }
    }
    
    public static void getAllNumbers(){
        System.out.println("Passport numbers: ");
        for (int i = 0; i < passList.size(); i++) {

            passList.get(i).print();
        }
    }
    
    public static void findLightest(){
        double min = passList.get(0).getWeightKg();
        int lightestIndex = 0;

        for (int i = 0; i < passList.size(); i++) {
            double val = passList.get(i).getWeightKg();

            if (val<min){
                min = val;
                lightestIndex = i;
            }

        }

        System.out.println("The lightest is: ");
        passList.get(lightestIndex).print();
    }
    
    public static void displayYounger(){
        if (!(passList.size()==0)){
            
            System.out.println("Enter age: ");
            int age = input.nextInt();
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int tempYob = year - age;

            System.out.println("The younger than the age you entered are:");
            for (int i = 0; i < passList.size(); i++) {

                    int val = passList.get(i).getYob();
                    if (tempYob<val){

                        passList.get(i).print();   
                }  
        }
        }
        else {
                System.out.println("No entries");
        }
    }
  
    public static void main(String[] args) {
        
        while (true) {
            
            userMenu();
            
            switch (inputValue){
                
                case 1:
                   getAllNumbers();
 
                   break;
                
                case 2:
                    sameCityPassengers();
                    break;
                
                case 3:
                    
                   getTallest();
                   break;
                    
                case 4:
                     
                    findLightest();
                    break;
                    
                case 5:
                    displayYounger();
                    break;
                case 6:
                    
                    addPassenger();
                    try {
                        Passport passport = new Passport(number, firstName, lastName, address, city, heightCm, weightKg, yob);
                        passList.add(passport);
                        System.out.println("You've created following passenger: "); 
                        passport.print();
                    } catch (Exception ex){
                            System.out.println
                                ("Error while creating an error: " + ex.getMessage());
                        }
                    break;
                    
                case 0:
                    
                    System.out.println("Exiting. Good bye!");
                    System.exit(1);
                    
                default:
                    
                    System.out.println("Please enter from 0-6");
            
        }
        
    }
    }  
}

