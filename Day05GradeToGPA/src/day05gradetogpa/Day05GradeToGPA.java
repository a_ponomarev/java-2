/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day05gradetogpa;

import java.util.Scanner;

/**
 *
 * @author 1796188
 */
public class Day05GradeToGPA {

   
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String mark = "";
        double grade=0;
        int count = 0;
        double sum = 0;
        
        while (true) {
            System.out.println("Please write down the mark (A, A+, B, B-): ");
            mark = input.nextLine().toLowerCase();
        
            if (mark.equals(""))
                break;
            
            switch (mark){
                case ("a"): 
                    grade = 4.0;

                    break;
                case ("a-"): 
                    grade = 3.7;
                   
                    break;
                case ("b+"): 
                    grade = 3.3;
 
                    break;
                case ("b"): 
                    grade = 3.0;
   
                    break;
                case ("b-"): 
                   grade = 2.7; 
                    count+=1;
                    sum+=grade;
                   break;
                case ("c+"): 
                    grade = 2.3;
 
                    break;
                case ("c"): 
                    grade = 2.0; 

                    break;
                case ("d"): 
                    grade = 1.0;

                    break;
                case ("f"): 
                    grade = 0;
                    
                     
                    break;
                
                default:
                    System.out.println("Incorrect value");
            } 
            System.out.printf("Numerical grade is %.2f\n", grade);
            sum+=grade;
            count++;
        }  
        
            System.out.println("You've entered " + count+" entries");
            System.out.println("The sum of the grades is " + sum);
            System.out.println("The GPA average is "+ sum/count);
    }
    
}
