
package day11peopleinh;

import java.util.ArrayList;

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
    public String toString(){
        return String.format("Person: %s, %d", name, age);
    }
}

class Teacher extends Person {
    String discipline;
    int yearsOfExp;

    public Teacher(String name, int age, String discipline, int yearsOfExp) {
        super(name, age);
        this.discipline = discipline;
        this.yearsOfExp = yearsOfExp;
    }
    
    public String toString(){
        return String.format("Teacher: %s, %d, %s, %d", name, age, discipline, yearsOfExp);
    }
}

class Student extends Person {
    String program;
    Double gpa;

    public Student(String name, int age, String program, Double gpa) {
        super(name, age);
        this.program = program;
        this.gpa = gpa;
    }
    
    public String toString(){
        return String.format("Student: %s, %d, %s, %f", name, age, program, gpa);
    }
}

public class Day11PeopleInh {
    static ArrayList<Person> peopleList = new ArrayList<>();
    
    public static void main(String[] args) {
       
       peopleList.add(new Person("Jerry", 33));
       peopleList.add(new Teacher("Maria", 44, "PhysEd", 22));
       peopleList.add(new Student("Larry", 27, "Nursing", 3.76));
       
        for (Person p: peopleList) {
            System.out.println(p);
        }
       
       
    }
    
}
