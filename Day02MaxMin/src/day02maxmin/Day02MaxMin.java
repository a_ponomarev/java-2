/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02maxmin;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 1796188
 */
public class Day02MaxMin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Double> numList = new ArrayList<>();
        double num;
        Scanner input = new Scanner(System.in);

        do {
            System.out.println("Please enter a positive number, 0 ends: ");
            num = input.nextDouble();
            if (num != 0) {
                numList.add(num);
            }

        } while (num != 0);

        double sum = 0;
        double max = numList.get(0);
        double min = numList.get(0);
        for (int i = 0; i < numList.size(); i++) {
            double n = numList.get(i);
            sum += n;

            if (n > max) {
                max = n;
            } else if (n < min) {
                min = n;
            }

        }
        System.out.println("Sum of all numbers: " + sum);
        System.out.println("Average of all numbers: " + sum / numList.size());
        System.out.println("The maximum of numbers: " + max);
        System.out.println("The minimum of numbers: " + min);
    }
}
