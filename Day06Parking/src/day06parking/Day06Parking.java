
package day06parking;

class Car {

    String color;
    double engineSizeLitres;
    String makeModel;
    int yearOfProduction;
    
    public Car(String c, double engineSizeLitres, String makeModel, int yearOfProduction) {
        color = c; // если имена совпадают и чтобы не запутаться применяют this если же имена разные this не нужен
        this.engineSizeLitres = engineSizeLitres;
        this.makeModel = makeModel;
        this.yearOfProduction = yearOfProduction;
    }
    
    void print() {
        System.out.printf("Car: %s, %.2f, %s, %d\n", color, engineSizeLitres, makeModel, yearOfProduction);
    }
    
}

public class Day06Parking {

    public static void main(String[] args) {
        Car c1 = new Car("red", 2.73, "Audi A8", 2017);
        c1.print();
        Car c2 = new Car("green", 1.73, "BMW", 2016);
        c2.print();
    }
    
}
